help:

setup: build

build:
	docker-compose build

_fe:
	cd frontend && npm run build

setup-docker:
	./get-docker.sh

start:
	docker-compose up

stop:
	docker-compose down

heroku_push: _fe
	cd backend && heroku container:push web

heroku_release:
	cd backend && heroku container:release web

deploy: heroku_push heroku_release
