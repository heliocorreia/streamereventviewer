Single Page Application aimed to integrate with Twitch, for:

- Log in and authorize app
- Subscribe favorite streamer to webhooks
- Show favorite streamer custom page

## Setup

Docker based setup

```
$ make setup    # docker-compose build
$ make start    # docker-compose run
$ make stop     # docker-compose stop
```

## Deployment / Scaling

The frontend app could be hosted like a static page at AWS S3 or Netlify.

Amazon Kinesis or Apache Kafka for ingesting Twitch webhooks data.

The backend could be deployed as an AWS API Gateway instance to handle the comunication between the clients and servers. The Connect, Disconnect and SendMessage operations could be handled via AWS Lambdas.

DynamoDB or Cassandra could be used to store the connection identifiers.

![](websockets-arch.png)
