"use strict";

const Hapi = require("hapi");
const request = require("request-promise");
const WebSocket = require("ws");

async function start() {
  const sessions = {};

  const server = Hapi.server({
    host: "0.0.0.0",
    port: process.env.PORT || 8000
  });

  server.route({
    method: "GET",
    path: "/webhook/",
    handler: (req, h) => {
      console.log(req.query);
      if (req.query["hub.challenge"]) {
        return req.query["hub.challenge"];
      }
      return null;
    }
  });

  server.route({
    method: "POST",
    path: "/webhook/",
    config: {
      payload: { output: "data", parse: true, allow: "application/json" }
    },
    handler: (req, h) => {
      console.log("callback payload...");
      console.log(JSON.stringify(req.payload));

      if (req.payload && req.payload.data) {
        (sessions[req.payload.data[0].to_id] || [])
          .filter(ws => ws.readyState === WebSocket.OPEN)
          .forEach(ws => ws.send(JSON.stringify(req.payload.data[0])));
      }

      return null;
    }
  });

  await server.register(require("inert"));

  server.route({
    method: "GET",
    path: "/{param*}",
    handler: {
      directory: {
        path: "./public/"
      }
    }
  });

  await server.register(require("hapi-plugin-websocket"));

  server.route({
    method: "POST",
    path: "/",
    config: {
      cors: { origin: ["*"] },
      plugins: {
        websocket: {
          only: true,
          autoping: 30 * 1000,
          disconnect: ({ ctx, ws }) => {
            // remove sessions by streamerId
            const session = sessions[ctx.streamerId] || [];
            sessions[ctx.streamerId] = session.filter(s => s !== ws);
          }
        }
      }
    },
    handler: async (req, h) => {
      let { ctx } = req.websocket();

      // get data by streamer login
      const streamer = await request({
        uri: "https://api.twitch.tv/helix/users",
        qs: {
          login: req.payload.streamer
        },
        headers: {
          "Client-ID": process.env.CLIENT_ID
        },
        json: true
      })
        .then(res => res.data)
        .catch(console.log);

      const streamerId = streamer[0].id;
      console.log(`streamer: ${req.payload.streamer}`, streamerId);

      // setup subscription
      await request({
        url: "https://api.twitch.tv/helix/webhooks/hub",
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "Client-ID": process.env.CLIENT_ID
        },
        body: {
          "hub.callback": `${
            process.env.TWITCH_WEBHOOKS_CALLBACK_BASE_URL
          }/webhook/`,
          "hub.mode": "subscribe",
          "hub.topic": `https://api.twitch.tv/helix/users/follows?first=1&to_id=${streamerId}`,
          "hub.lease_seconds": process.env.TWITCH_WEBHOOKS_LEASE_SECONDS,
          "hub.secret": process.env.TWITCH_WEBHOOKS_SECRET
        },
        json: true
      }).catch(console.log);

      console.log(`sessions before: ${Object.keys(sessions)}`);

      // save sessions by streamerId
      ctx.streamerId = streamerId;
      sessions[ctx.streamerId] = sessions[ctx.streamerId] || [];
      sessions[ctx.streamerId].push(req.websocket().ws);

      console.log(`sessions after: ${Object.keys(sessions)}`);

      return null;
    }
  });

  try {
    await server.start();
  } catch (err) {
    console.log(err);
    process.exit(1);
  }

  console.log("Server running at:", server.info.uri);
}

start();
