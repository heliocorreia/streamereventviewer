import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import AccountCircle from "@material-ui/icons/AccountCircle";
import { TWITCH_OAUTH_STATE, TWITCH_OAUTH_TOKEN } from "./constants";
import { randomChars } from "./utils";

class Account extends Component {
  componentDidMount() {
    this.checkAccessToken();
  }

  checkAccessToken() {
    const token = document.location.hash.match(/access_token=(\w+)/);
    if (!token) {
      return;
    }

    const state = document.location.hash.match(/state=(\w+)/);
    if (!state) {
      return;
    }

    if (sessionStorage.getItem(TWITCH_OAUTH_STATE) === state[1]) {
      sessionStorage.setItem(TWITCH_OAUTH_TOKEN, token[1]);
    }

    document.location.replace("/");
  }

  hasToken() {
    return !!sessionStorage.getItem(TWITCH_OAUTH_TOKEN);
  }

  render() {
    return <>{this.hasToken() ? <LoggedIn /> : <LoggedOut />}</>;
  }
}

const LoggedIn = () => (
  <IconButton color="inherit">
    <AccountCircle />
  </IconButton>
);

const LoggedOut = () => {
  const onClick = () => {
    sessionStorage.setItem(TWITCH_OAUTH_STATE, randomChars(16));

    const params = {
      client_id: process.env.REACT_APP_CLIENT_ID,
      redirect_uri: process.env.REACT_APP_AUTH_REDIRECT_URI,
      response_type: "token",
      state: sessionStorage.getItem(TWITCH_OAUTH_STATE),
      scope: "openid"
    };
    const qs = Object.keys(params)
      .map(k => k + "=" + params[k])
      .join("&");
    const url = `${process.env.REACT_APP_TWITCH_AUTH_ENDPOINT}?${qs}`;

    window.location.href = url;
  };

  return (
    <Button color="inherit" onClick={onClick}>
      Login
    </Button>
  );
};

export default Account;
