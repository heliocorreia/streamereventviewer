import React from "react";
import renderer from "react-test-renderer";
import Account from "./Account";

it("renders logged out", () => {
  sessionStorage.setItem("twitchOAuthToken", null);
  const tree = renderer.create(<Account />).toJSON();
  expect(tree).toMatchSnapshot();
});

it("renders logged in", () => {
  sessionStorage.setItem("twitchOAuthToken", "abc");
  const tree = renderer.create(<Account />).toJSON();
  expect(tree).toMatchSnapshot();
});
