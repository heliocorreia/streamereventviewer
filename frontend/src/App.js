import React, { Component } from "react";
import PropTypes from "prop-types";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import HomeIcon from "@material-ui/icons/Home";
import RssFeedIcon from "@material-ui/icons/RssFeed";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import Fade from "@material-ui/core/Fade";
import Account from "./Account";
import FormDialog from "./FormDialog";
import Streamer from "./Streamer";

const styles = {
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  }
};

class MainBar extends Component {
  state = {
    anchorEl: null
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { classes } = this.props;
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);

    return (
      <Router>
        <div className={classes.root}>
          <AppBar position="static">
            <Toolbar>
              <div>
                <IconButton
                  className={classes.menuButton}
                  color="inherit"
                  aria-label="Menu"
                  onClick={this.handleClick}
                >
                  <MenuIcon />
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: "top",
                    horizontal: "right"
                  }}
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "right"
                  }}
                  open={open}
                  onClose={this.handleClose}
                  TransitionComponent={Fade}
                >
                  <MenuItem
                    component={Link}
                    to="/"
                    selected={false}
                    onClick={this.handleClose}
                    className={classes.menuItem}
                  >
                    <ListItemIcon>
                      <HomeIcon />
                    </ListItemIcon>
                    <ListItemText primary="Home" />
                  </MenuItem>
                  <MenuItem
                    component={Link}
                    to="/streamer"
                    selected={false}
                    onClick={this.handleClose}
                    className={classes.menuItem}
                  >
                    <ListItemIcon>
                      <RssFeedIcon />
                    </ListItemIcon>
                    <ListItemText primary="Streamer" />
                  </MenuItem>
                </Menu>
              </div>
              <Typography
                variant="title"
                color="inherit"
                className={classes.grow}
              >
                StreamerEventViewer
              </Typography>
              <Account />
            </Toolbar>
          </AppBar>

          <Route path="/" exact={true} component={FormDialog} />
          <Route path="/streamer" component={Streamer} />
        </div>
      </Router>
    );
  }
}

MainBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(MainBar);
