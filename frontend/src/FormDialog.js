import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { TWITCH_OAUTH_TOKEN, TWITCH_FAVORITE_STREAMER_NAME } from "./constants";

export default class FormDialog extends Component {
  state = {
    open: false,
    textValue: sessionStorage.getItem(TWITCH_FAVORITE_STREAMER_NAME)
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleSave = () => {
    this.setState({ open: false }, this.handleAfterSave);
  };

  handleAfterSave = () => {
    let streamer = this.state.textValue;

    sessionStorage.setItem(TWITCH_FAVORITE_STREAMER_NAME, streamer);
  };

  handleTextFieldChange = e => {
    this.setState({
      textValue: e.target.value
    });
  };

  render() {
    if (!sessionStorage.getItem(TWITCH_OAUTH_TOKEN)) {
      return null;
    }

    return (
      <div>
        <Button onClick={this.handleClickOpen}>
          Click to open your settings
        </Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Twitch</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Please, inform your favorite Twitch streamer name:
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="name"
              label="Name"
              type="text"
              fullWidth
              defaultValue={this.state.textValue}
              onChange={this.handleTextFieldChange}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose}>Cancel</Button>
            <Button onClick={this.handleSave} color="primary">
              Save
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
