import React from "react";
import renderer from "react-test-renderer";
import FormDialog from "./FormDialog";
import { TWITCH_OAUTH_TOKEN } from "./constants";

it("does not render without OAuth Token", () => {
  sessionStorage.removeItem(TWITCH_OAUTH_TOKEN);
  const tree = renderer.create(<FormDialog />).toJSON();
  expect(tree).toMatchSnapshot();
});

it("renders form dialog", () => {
  sessionStorage.setItem(TWITCH_OAUTH_TOKEN, "abc");
  const tree = renderer.create(<FormDialog />).toJSON();
  expect(tree).toMatchSnapshot();
});
