import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { TWITCH_FAVORITE_STREAMER_NAME } from "./constants";

const Streamer = props => {
  const streamer = sessionStorage.getItem(TWITCH_FAVORITE_STREAMER_NAME);

  if (!streamer) {
    return (
      <p>
        In order to see the embedded content, you need to setup your favorite
        streamer name at <Link to="/">home page</Link> settings dialog.
      </p>
    );
  }

  const ws = new WebSocket(`${process.env.REACT_APP_WS_BASE_URL}/`);
  const [state, setState] = useState({ activities: [] });

  useEffect(() => {
    const handleOpen = () => {
      ws.send(JSON.stringify({ streamer }));
    };

    const handleMessage = msg => {
      if (msg.data) {
        setState({
          activities: [...state.activities.slice(-9), JSON.parse(msg.data)]
        });
      }
    };

    ws.addEventListener("open", handleOpen);
    ws.addEventListener("message", handleMessage);

    return () => {
      ws.removeEventListener("open", handleOpen);
      ws.removeEventListener("message", handleMessage);
    };
  }, [state]);

  return (
    <div>
      <iframe
        src={`https://player.twitch.tv/?${streamer}`}
        height="300"
        width="100%"
        frameBorder="0"
        scrolling="no"
        allowFullScreen="no"
        title="video"
        referrerPolicy="no-referrer"
      />
      <iframe
        frameBorder="0"
        scrolling="no"
        id="chat_embed"
        src={`https://twitch.tv/embed/${streamer}/chat`}
        height="400"
        width="100%"
        title="chat"
        referrerPolicy="no-referrer"
      />
      <div>
        <h1>Last 10 follows events</h1>
        <div
          style={{
            display: "flex",
            flexDirection: "column-reverse"
          }}
        >
          {state.activities.map((d, i) => (
            <div key={i}>
              {d.from_name} was followed by {d.to_name} at {d.followed_at}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Streamer;
