export const TWITCH_OAUTH_STATE = "twitchOAuthState";
export const TWITCH_OAUTH_TOKEN = "twitchOAuthToken";
export const TWITCH_FAVORITE_STREAMER_NAME = "twitchFavStreamer";
