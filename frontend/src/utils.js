export const randomChars = length => {
  // Browser Grid Support
  // https://caniuse.com/#feat=cryptography
  const charset =
    "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_";
  let result = [];

  while (length > 0) {
    let random = crypto.getRandomValues(new Uint8Array(16));
    let len = random.length;

    while (len--) {
      if (length === 0) {
        break;
      }
      if (random[len] < charset.length) {
        result.push(charset[random[len]]);
        length--;
      }
    }
  }

  return result.join("");
};
